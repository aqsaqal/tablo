package com.tablo.test.api;

import com.tablo.test.service.TabloService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TabloController {

  private final TabloService tabloService;

  @GetMapping("api/code")
  public String generateNewCode() {
    return tabloService.generateNewCode();
  }
}
