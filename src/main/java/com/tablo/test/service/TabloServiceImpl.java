package com.tablo.test.service;

import com.tablo.test.model.Tablo;
import com.tablo.test.repository.TabloRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class TabloServiceImpl implements TabloService {

  private final TabloRepository tabloRepository;

  @Override
  public String generateNewCode() {
    Tablo tablo = tabloRepository.getReferenceById(1L);
    String code = tablo.getCode();
    log.info("generating new code from: {}", code);

    String newCode = generateNewCode(code);
    tablo.setCode(newCode);
    tabloRepository.save(tablo);
    log.info("generated new code: {}", newCode);

    return newCode;
  }

  public String generateNewCode(String prevCode) {
    String newCode = "";
    boolean increment = true;
    char[] charArray = prevCode.toCharArray();

    for (int i = charArray.length-1; i >= 0; i--) {
      char character = charArray[i];
      if (increment) {
        if (character >= '0' && character <= '9') {
          character += 1;
          if (!(character >= '0' && character <= '9')) {
            character = '0';
          } else {
            increment = false;
          }
        } else if (character >= 'a' && character <= 'z') {
          character += 1;
          if (!(character >= 'a' && character <= 'z')) {
            character = 'a';
          } else {
            increment = false;
          }
        }
      }

      newCode =  character + newCode;
    }

    if (increment) {
      newCode = "a0" + newCode;
    }

    return newCode;
  }
}

