package com.tablo.test.repository;

import com.tablo.test.model.Tablo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TabloRepository extends JpaRepository<Tablo, Long>, JpaSpecificationExecutor {

}
