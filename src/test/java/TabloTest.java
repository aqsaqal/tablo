import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.tablo.test.model.Tablo;
import com.tablo.test.repository.TabloRepository;
import com.tablo.test.service.TabloService;
import com.tablo.test.service.TabloServiceImpl;
import org.junit.jupiter.api.Test;

public class TabloTest {

  private TabloRepository tabloRepository = mock(TabloRepository.class);
  private TabloService tabloService = new TabloServiceImpl(tabloRepository);

  @Test
  void testTabloBeginFourChars() {
    Tablo initial = new Tablo();
    initial.setCode("a0a0");
    when(tabloRepository.getReferenceById(anyLong())).thenReturn(initial);

    String expected = "a0a1";
    String actual = tabloService.generateNewCode();

    assert expected.equals(actual);
  }

  @Test
  void testTabloEndFourChars() {
    Tablo initial = new Tablo();
    initial.setCode("z9z9");
    when(tabloRepository.getReferenceById(anyLong())).thenReturn(initial);

    String expected = "a0a0a0";
    String actual = tabloService.generateNewCode();

    assert expected.equals(actual);
  }


  @Test
  void testTabloBeginSixChars() {
    Tablo initial = new Tablo();
    initial.setCode("a0a0a0");
    when(tabloRepository.getReferenceById(anyLong())).thenReturn(initial);

    String expected = "a0a0a1";
    String actual = tabloService.generateNewCode();

    assert expected.equals(actual);
  }


  @Test
  void testTabloEndSixChars() {
    Tablo initial = new Tablo();
    initial.setCode("z9z9z9");
    when(tabloRepository.getReferenceById(anyLong())).thenReturn(initial);

    String expected = "a0a0a0a0";
    String actual = tabloService.generateNewCode();

    assert expected.equals(actual);
  }
}
